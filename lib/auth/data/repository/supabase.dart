import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(String email, String password) async {
  return await supabase.auth.signUp(email: email, password: password);
}

Future<AuthResponse> signIn(String email, String password) async {
  return await supabase.auth.signInWithPassword(email: email, password: password);
}

Future<void> signOut() async {
  return await supabase.auth.signOut();
}

Future<void> sendOTP(String email) async {
  return await supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOTP(String email, String code) async {
  return await supabase.auth.verifyOTP(email: email, token: code, type: OtpType.email);
}

Future<UserResponse> changePassword(String password) async {
  return await supabase.auth.updateUser(UserAttributes(
    password: password
  ));
}