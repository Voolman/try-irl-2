import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:training2/auth/domain/OTPVerifPresenter.dart';
import 'package:training2/auth/presetation/LogIn.dart';
import 'package:training2/auth/presetation/NewPassword.dart';
import 'package:training2/common/utils.dart';
import 'package:training2/common/widgets/CustomTextField.dart';

import '../domain/ForgotPresenter.dart';
import 'ForgotPassword.dart';

class OTPVerification extends StatefulWidget {
  const OTPVerification({super.key});

  @override
  State<OTPVerification> createState() => _OTPVerificationState();
}

class _OTPVerificationState extends State<OTPVerification> {
  @override
  Widget build(BuildContext context) {
    TextEditingController code = TextEditingController();
    return Scaffold(
        body: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Верификация',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleLarge,
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Text(
                          'Введите 6-ти значный код из письма',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleMedium,
                        ),
                      ],
                    ),
                    const SizedBox(height: 58),
                    Pinput(
                      length: 6,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      controller: code,
                      defaultPinTheme: PinTheme(
                        height: 32,
                        width: 32,
                        decoration: BoxDecoration(
                          border: Border.all(color: colors.subtext),
                          borderRadius: BorderRadius.zero
                        )
                      ),
                      submittedPinTheme: PinTheme(
                          height: 32,
                          width: 32,
                          decoration: BoxDecoration(
                              border: Border.all(color: colors.accent),
                              borderRadius: BorderRadius.zero
                          )
                      ),
                    ),
                    const SizedBox(height: 48),
                    GestureDetector(
                      onTap: (){
                        pressForgot(
                            email.text,
                                (){},
                                (String e){showError(context, e);}
                        );
                      },
                      child: Text(
                        'Получить новый код',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent),
                      ),
                    ),
                    const SizedBox(height: 445),
                    SizedBox(
                      height: 46,
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            pressVerify(
                                email.text,
                                code.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const NewPassword()));},
                            (String e){showError(context, e);}
                            );
                          },
                          style: Theme
                              .of(context)
                              .filledButtonTheme
                              .style,
                          child: Text(
                            'Сбросить пароль',
                            style: Theme
                                .of(context)
                                .textTheme
                                .labelMedium,
                          )
                      ),
                    ),
                    const SizedBox(height: 14),
                    GestureDetector(
                      onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LogIn()));},
                      child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "Я вспомнил свой пароль! ",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(fontWeight: FontWeight.w700)
                          ),
                          TextSpan(
                              text: 'Вернуться',
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(fontWeight: FontWeight.w400,
                                  color: colors.accent)
                          )
                        ],
                      ),
                      ),
                    )
                  ],
                ),
              )
            ]
        )
    );
  }
}