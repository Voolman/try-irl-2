import 'package:flutter/material.dart';
import 'package:training2/auth/domain/SignUpPresenter.dart';
import 'package:training2/auth/presetation/LogIn.dart';
import 'package:training2/common/widgets/CustomTextField.dart';

import '../../common/utils.dart';
import 'Holder.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    TextEditingController email = TextEditingController();
    TextEditingController password = TextEditingController();
    TextEditingController confirmPassword = TextEditingController();
    void onChanged(_) {

    }
    return Scaffold(
        body: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Создать аккаунт',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleLarge,
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Text(
                          'Завершите регистрацию чтобы начать',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleMedium,
                        ),
                      ],
                    ),
                    const SizedBox(height: 4,),
                    CustomTextField(
                        label: "Почта",
                        hint: '***********@mail.com',
                        controller: email,
                        onChanged: onChanged
                    ),
                    CustomTextField(
                        label: "Пароль",
                        hint: '**********',
                        enableObscure: true,
                        controller: password,
                        onChanged: onChanged
                    ),
                    CustomTextField(
                        label: "Повторите пароль",
                        hint: '**********',
                        enableObscure: true,
                        controller: confirmPassword,
                        onChanged: onChanged
                    ),
                    const SizedBox(height: 319),
                    SizedBox(
                      height: 46,
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            pressSignUp(
                                email.text,
                                password.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                                    (String e){showError(context, e);}
                            );
                          },
                          style: Theme
                              .of(context)
                              .filledButtonTheme
                              .style,
                          child: Text(
                            'Зарегистрироваться',
                            style: Theme
                                .of(context)
                                .textTheme
                                .labelMedium,
                          )
                      ),
                    ),
                    const SizedBox(height: 14),
                    GestureDetector(
                      onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LogIn()));},
                      child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "У меня уже есть аккаунт! ",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(fontWeight: FontWeight.w700)
                          ),
                          TextSpan(
                              text: 'Войти',
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(fontWeight: FontWeight.w400,
                                  color: colors.accent)
                          )
                        ],
                      ),
                      ),
                    )
                  ],
                ),
              )
            ]
        )
    );
  }
}