import 'package:flutter/material.dart';
import 'package:training2/auth/domain/LogInPresenter.dart';
import 'package:training2/auth/presetation/ForgotPassword.dart';
import 'package:training2/common/utils.dart';
import 'package:training2/auth/presetation/Holder.dart';
import 'package:training2/auth/presetation/SignUp.dart';
import 'package:training2/common/widgets/CustomTextField.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  @override
  Widget build(BuildContext context) {
    TextEditingController email = TextEditingController();
    TextEditingController password = TextEditingController();
    bool isRemember = false;
    void onChange(_){

    }
    return Scaffold(
        body: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Добро пожаловать',
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Text(
                          'Заполните почту и пароль чтобы продолжить',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                      ],
                    ),
                    const SizedBox(height: 4,),
                    CustomTextField(
                        label: "Почта",
                        hint: '***********@mail.com',
                        controller: email,
                        onChanged: onChange
                    ),
                    CustomTextField(
                        label: "Пароль",
                        hint: '**********',
                        enableObscure: true,
                        controller: password,
                        onChanged: onChange
                    ),
                    const SizedBox(height: 18),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              height: 22,
                              width: 22,
                              child: Checkbox(
                                  value: isRemember,
                                  activeColor: colors.accent,
                                  checkColor: colors.background,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)
                                  ),
                                  side: BorderSide(color: colors.subtext, width: 1),
                                  onChanged: (newValue){
                                    setState(() {
                                      isRemember = newValue!;
                                    });
                                  }
                              ),
                            ),
                            const SizedBox(width:8),
                            Text(
                              'Запомнить меня',
                              style: Theme.of(context).textTheme.titleMedium,
                            )
                          ],
                        ),
                        GestureDetector(
                          onTap: (){
                            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const ForgotPassword()));
                          },
                          child: Text(
                            'Забыли пароль?',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(color: colors.accent),
                          ),
                        )
                      ],
                    ),

                    const SizedBox(height: 371),
                    SizedBox(
                      height: 46,
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            pressSignIn(
                                email.text,
                                password.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                                (String e){showError(context, e);}
                            );
                          },
                          style: Theme.of(context).filledButtonTheme.style,
                          child: Text(
                            'Войти',
                            style: Theme.of(context).textTheme.labelMedium,
                          )
                      ),
                    ),
                    const SizedBox(height: 14),
                    GestureDetector(
                      onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SignUp()));},
                      child: RichText(
                        text: TextSpan(
                        children: [
                          TextSpan(
                              text: "У меня нет аккаунта! ",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                          ),
                          TextSpan(
                              text: 'Создать',
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: colors.accent)
                          )
                        ],
                      ),
                      ),
                    )
                  ],
                ),
              )
            ]
        )
    );
  }
}