import 'dart:io';
import 'package:flutter/material.dart';
import 'package:training2/auth/domain/HolderPresenter.dart';
import 'package:training2/common/utils.dart';


class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}

class _HolderState extends State<Holder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 399, horizontal: 24),
          child: SizedBox(
            height: 46,
            width: double.infinity,
            child: FilledButton(
                onPressed: (){
                  pressSignOut(
                      (){exit(0);},
                      (String e){showError(context, e);}
                  );
                },
                style: Theme
                    .of(context)
                    .filledButtonTheme
                    .style,
                child: Text(
                  'ВЫХОД',
                  style: Theme
                      .of(context)
                      .textTheme
                      .labelMedium,
                )
            ),
          ),
        )
    );
  }
}