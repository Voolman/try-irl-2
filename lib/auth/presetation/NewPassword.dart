import 'package:flutter/material.dart';
import 'package:training2/auth/domain/NewPasswordPresenter.dart';
import 'package:training2/auth/presetation/LogIn.dart';
import 'package:training2/common/utils.dart';
import 'package:training2/common/widgets/CustomTextField.dart';
import 'Holder.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});

  @override
  State<NewPassword> createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {
  @override
  Widget build(BuildContext context) {
    TextEditingController password = TextEditingController();
    TextEditingController confirmPassword = TextEditingController();
    void onChanged(_) {

    }
    return Scaffold(
        body: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Новый пароль',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleLarge,
                        ),
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Text(
                          'Введите новый пароль',
                          style: Theme
                              .of(context)
                              .textTheme
                              .titleMedium,
                        ),
                      ],
                    ),
                    const SizedBox(height: 4,),
                    CustomTextField(
                        label: "Пароль",
                        hint: '**********',
                        enableObscure: true,
                        controller: password,
                        onChanged: onChanged
                    ),
                    CustomTextField(
                        label: "Повторите пароль",
                        hint: '**********',
                        enableObscure: true,
                        controller: confirmPassword,
                        onChanged: onChanged
                    ),
                    const SizedBox(height: 411),
                    SizedBox(
                      height: 46,
                      width: double.infinity,
                      child: FilledButton(
                          onPressed: (){
                            pressNew(
                                password.text,
                                (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                                (String e){showError(context, e);}
                            );
                          },
                          style: Theme
                              .of(context)
                              .filledButtonTheme
                              .style,
                          child: Text(
                            'Подтвердить',
                            style: Theme
                                .of(context)
                                .textTheme
                                .labelMedium,
                          )
                      ),
                    ),
                    const SizedBox(height: 14),
                    GestureDetector(
                      onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const LogIn()));},
                      child: RichText(text: TextSpan(
                        children: [
                          TextSpan(
                              text: "Я вспомнил свой пароль! ",
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(fontWeight: FontWeight.w700)
                          ),
                          TextSpan(
                              text: 'Вернуться',
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleMedium
                                  ?.copyWith(fontWeight: FontWeight.w400,
                                  color: colors.accent)
                          )
                        ],
                      ),
                      ),
                    )
                  ],
                ),
              )
            ]
        )
    );
  }
}