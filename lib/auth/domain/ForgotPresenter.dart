import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/repository/supabase.dart';

Future<void> pressForgot(String email, Function onResponse, Function(String) onError) async {
  try{
    await sendOTP(email);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }
}