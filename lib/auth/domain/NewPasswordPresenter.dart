import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training2/auth/data/repository/supabase.dart';

Future<void> pressNew(String password, Function onResponse, Function onError) async {
  try{
    await changePassword(password);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }
}