import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/repository/supabase.dart';

Future<void> pressVerify(String email, String code, Function onResponse, Function onError) async {
  try{
    await verifyOTP(email, code);
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }
}