import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:training2/auth/data/repository/supabase.dart';

Future<void> pressSignOut(Function onResponse, Function onError) async {
  try{
    await signOut();
    onResponse();
  }on AuthException catch(e){
    onError(e.message);
  }
}