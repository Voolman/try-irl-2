import 'package:flutter/material.dart';
import 'package:training2/common/colors.dart';

var colors = LightColors();
var theme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: 24,
      color: colors.text
    ),
    titleMedium: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 14,
        color: colors.subtext
    ),
    labelMedium: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: 16,
        color: colors.disableTextAccent
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: colors.accent,
      disabledBackgroundColor: colors.disableAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      )
    )
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: colors.subtext, width: 1),
      borderRadius: BorderRadius.circular(4)
    ),
  )
);