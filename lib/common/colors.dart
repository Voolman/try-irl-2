import 'package:flutter/material.dart';

abstract class ColorsApp{
  abstract final Color text;
  abstract final Color accent;
  abstract final Color hint;
  abstract final Color background;
  abstract final Color error;
  abstract final Color subtext;
  abstract final Color textAccent;
  abstract final Color disableTextAccent;
  abstract final Color block;
  abstract final Color disableAccent;
}

class LightColors extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => const Color.fromARGB(255, 106, 139, 249);

  @override
  // TODO: implement background
  Color get background => const Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement block
  Color get block => const Color.fromARGB(255, 242, 242, 242);

  @override
  // TODO: implement disableTextAccent
  Color get disableTextAccent => const Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement error
  Color get error => const Color.fromARGB(255, 255, 0, 0);

  @override
  // TODO: implement hint
  Color get hint => const Color.fromARGB(255, 207, 207, 207);

  @override
  // TODO: implement subtext
  Color get subtext => const Color.fromARGB(255, 129, 129, 129);

  @override
  // TODO: implement text
  Color get text => const Color.fromARGB(255, 58, 58, 58);

  @override
  // TODO: implement textAccent
  Color get textAccent => const Color.fromARGB(255, 255, 255, 255);

  @override
  // TODO: implement disableAccent
  Color get disableAccent => Color.fromARGB(255, 167, 167, 167);
}